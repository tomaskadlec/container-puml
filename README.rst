container-puml
==============

.. _Docker: http://docker.com/
.. _PlantUML: http://plantuml.com/
.. _`Gitlab CI`: https://about.gitlab.com/gitlab-ci/
.. _`Debian testing`: https://wiki.debian.org/DebianTesting

The project provides a Docker_ image with PlantUML_ installed. The image is based on
`Debian testing`_. The image is build automatically via `Gitlab CI`_

.. WARNING::

   Don't forget to run PlantUML_ with ``-Djava.awt.headless=true``.

