#!/bin/bash

function usage {
    cat << --USAGE--

Usage: $0 ARGUMENTS

    If no arguments are given PlantUML help is displayed.

--USAGE--
}

[ $# -lt 1 ] && {
    usage;
    set -- '-h'
}

SCRIPT_DIR=$(readlink -f "$0")
SCRIPT_DIR=$(dirname "$SCRIPT_DIR")

docker run -ti --rm \
	--volume="$HOME:$HOME" \
	--volume="/etc/group:/etc/group:ro" \
	--volume="/etc/passwd:/etc/passwd:ro" \
	--workdir="$SCRIPT_DIR" \
	--user="$UID:$(id -g)" \
	registry.gitlab.com/tomaskadlec/container-puml \
    java -Djava.awt.headless=true -jar /usr/local/plantuml/plantuml.jar "$@"    
