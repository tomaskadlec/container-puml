FROM debian:testing
MAINTAINER Tomas Kadlec <tomas@tomaskadlec.net>

RUN apt-get update; \
    apt-get -y install default-jre graphviz wget make

RUN mkdir -p /usr/local/plantuml; \
    wget -O /usr/local/plantuml/plantuml.jar https://downloads.sourceforge.net/project/plantuml/plantuml.8054.jar

CMD /bin/bash
